package main

type QuestionList []Question

// Erzeugt und liefert eine QuestionList.
func MakeQuestionList(questions ...Question) QuestionList {
	return QuestionList(questions)
}

// Liefert alle Fragen aus der gegebenen Frageliste, die genau n Antwortmöglichkeiten haben.
func QuestionsWithNAnswers(ql QuestionList, n int) QuestionList {
	pred := func(q Question) bool { return len(q.answerChoices) == n }
	return FilterQuestions(ql, pred)
}

// Liefert alle Fragen aus der gegebenen Frageliste, die mindestens n Antwortmöglichkeiten haben.
func QuestionsWithAtLeastNAnswers(ql QuestionList, n int) QuestionList {
	pred := func(q Question) bool { return len(q.answerChoices) >= n }
	return FilterQuestions(ql, pred)
}

// Liefert alle Fragen aus der gegebenen Frageliste, die höchstens n Antwortmöglichkeiten haben.
func QuestionsWithAtMostNAnswers(ql QuestionList, n int) QuestionList {
	pred := func(q Question) bool { return len(q.answerChoices) <= n }
	return FilterQuestions(ql, pred)
}

// Filtert alle ungültigen Fragen aus der Liste heraus und liefert das Ergebnis.
func FilterInvalidQuestions(ql QuestionList) QuestionList {
	pred := func(q Question) bool { return q.IsValid() }
	return FilterQuestions(ql, pred)

}

/* Hilfsfunktionen für QuestionList.                   *
 * Diese Funktionen sollten zuerst implementiert und   *
 * dann für die obigen Funktionen genutzt werden       */

// Filtert eine Frageliste mit Hilfe des angegebenen Prädikats.
// D.h. liefert eine neue Liste, die nur die Elemente el enthält,
// fuer die pred(el)==true ist.
func FilterQuestions(ql QuestionList, pred func(q Question) bool) QuestionList {
	result := MakeQuestionList()
	for _, q := range ql {
		if pred(q) {
			result = append(result, q)
		}
	}
	return result
}

// Wendet die gegebene Funktion f() auf jede einzelne Frage einer Frageliste an.
// Liefert eine neue Liste mit den Ergebnissen.
// Die Funktion f() muss eine Frage erwarten und einen String liefern.
func MapQuestionsToStrings(ql QuestionList, f func(q Question) string) []string {
	result := make([]string, 0)
	for _, q := range ql {
		result = append(result, f(q))
	}
	return result
}

// Wendet die gegebene Funktion f() auf jede einzelne Frage einer Frageliste an.
// Liefert keine neue Liste. Die Funktion f() muss einen Question-Pointer erwarten
// und die Frage verändern.
func ForEachQuestion(ql QuestionList, f func(q *Question)) {
	for i := range ql {
		f(&ql[i])
	}
}
