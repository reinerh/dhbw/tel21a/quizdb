package main

import "fmt"

// Hilfsfunktion für Tests, liefert eine QuestionList.
func questionList1() QuestionList {
	q1 := MakeQuestion(
		"Was ist eine Katze?", 2,
		"Eine Maus", "Ein bösartiges Raubtier", "Ein knuddeliges Haustier", "Etwas anderes",
	)
	q2 := MakeQuestion(
		"Was ist ein Hund?", 1,
		"Eine Katze", "Der beste Freund des Menschen", "Ein Flohzirkus",
	)
	q3 := MakeQuestion(
		"Wie viele Antworten hat diese Frage?", 4,
		"1", "2", "3", "4", "5",
	)
	q4 := MakeQuestion(
		"Gibt es überhaupt Antworten?", 0,
	)
	return MakeQuestionList(q1, q2, q3, q4)
}

// Hilfsfunktion für Tests, gibt eine QuestionList aus.
func printQuestions(ql QuestionList) {
	for _, q := range ql {
		fmt.Println(q)
	}
	fmt.Println()
}

func ExampleQuestionsWithNAnswers() {
	printQuestions(QuestionsWithNAnswers(questionList1(), 3))
	printQuestions(QuestionsWithNAnswers(questionList1(), 4))
	printQuestions(QuestionsWithNAnswers(questionList1(), 5))

	// Output:
	// {Was ist ein Hund? [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	//
	// {Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	//
	// {Wie viele Antworten hat diese Frage? [1 2 3 4 5] 4}
	//
}

func ExampleQuestionsWithAtLeastNAnswers() {
	printQuestions(QuestionsWithAtLeastNAnswers(questionList1(), 3))
	printQuestions(QuestionsWithAtLeastNAnswers(questionList1(), 4))
	printQuestions(QuestionsWithAtLeastNAnswers(questionList1(), 5))

	// Output:
	// {Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	// {Was ist ein Hund? [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	// {Wie viele Antworten hat diese Frage? [1 2 3 4 5] 4}
	//
	// {Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	// {Wie viele Antworten hat diese Frage? [1 2 3 4 5] 4}
	//
	// {Wie viele Antworten hat diese Frage? [1 2 3 4 5] 4}
	//
}

func ExampleQuestionsWithAtMostNAnswers() {
	printQuestions(QuestionsWithAtMostNAnswers(questionList1(), 3))
	printQuestions(QuestionsWithAtMostNAnswers(questionList1(), 4))
	printQuestions(QuestionsWithAtMostNAnswers(questionList1(), 5))

	// Output:
	// {Was ist ein Hund? [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	// {Gibt es überhaupt Antworten? [] 0}
	//
	// {Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	// {Was ist ein Hund? [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	// {Gibt es überhaupt Antworten? [] 0}
	//
	// {Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	// {Was ist ein Hund? [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	// {Wie viele Antworten hat diese Frage? [1 2 3 4 5] 4}
	// {Gibt es überhaupt Antworten? [] 0}
	//
}

func ExampleFilterInvalidQuestions() {
	printQuestions(FilterInvalidQuestions(questionList1()))

	// Output:
	// {Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	// {Was ist ein Hund? [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	// {Wie viele Antworten hat diese Frage? [1 2 3 4 5] 4}
}

func ExampleFilterQuestions() {
	ql1 := questionList1()
	pred1 := func(q Question) bool { return q.correctAnswer == 2 }
	res1 := FilterQuestions(ql1, pred1)
	fmt.Println(res1)

	// Output:
	// [{Was ist eine Katze? [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}]
}

func ExampleMapQuestionsToStrings() {
	ql1 := questionList1()
	f1 := func(q Question) string { return q.QuestionText() }
	res1 := MapQuestionsToStrings(ql1, f1)
	fmt.Println(res1)

	// Output:
	// [Was ist eine Katze? Was ist ein Hund? Wie viele Antworten hat diese Frage? ]
}

func ExampleForEachQuestion() {
	ql1 := questionList1()
	f1 := func(q *Question) { q.questionText += "!!!" }
	ForEachQuestion(ql1, f1)
	printQuestions(ql1)

	// Output:
	// {Was ist eine Katze?!!! [Eine Maus Ein bösartiges Raubtier Ein knuddeliges Haustier Etwas anderes] 2}
	// {Was ist ein Hund?!!! [Eine Katze Der beste Freund des Menschen Ein Flohzirkus] 1}
	// {Wie viele Antworten hat diese Frage?!!! [1 2 3 4 5] 4}
	// {Gibt es überhaupt Antworten?!!! [] 0}
}
