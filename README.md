# Quiz-Datenbank

Aufgaben zur Erstellung einer Quiz-Datenbank.

In der Datei `questions.go` ist ein Datentyp mitsamt einiger Funktionen definiert, die für die Fragen in einem Quiz verwendet werden können.
Die Datei `questions_test.go` enthält entsprechende Tests.

Die Datei `questionlist.go` enthält einen Datentyp für eine Liste von Fragen sowie einige Funktionsrümpfe für Funktionen, die für die Verwaltung einer Fragendatenbank nützlich wären.
Ihre Aufgabe ist es, diese Funktionen zu implementieren.

**Hinweise:**
* Die Datei `questionlist_test.go` enthält die zugehörigen Tests, dieses sollten Sie zum Laufen bringen.
* Die Funktionen ganz unten in `questionlist.go` sind zentrale Werkzeuge für die Funktionen darüber.
  Sie sollten diese zuerst implementieren.