package main

import (
	"fmt"
	"strings"
)

// Ein Datentyp für eine Frage mitsamt einer Liste von Antworten.
type Question struct {
	questionText  string
	answerChoices []string
	correctAnswer int
}

// Prüft, ob eine Frage gültig ist.
// Eine Frage ist genau dann gültig, wenn es mindestens eine Antwortmöglichkeit gibt
// und wenn die Nummer der korrekte Antwort einer der Antwortmöglichkeiten entspricht.
func (q Question) IsValid() bool {
	return q.correctAnswer >= 0 && len(q.answerChoices) > q.correctAnswer
}

// Liefert den Text der Frage, falls diese gültig ist.
// Liefert ansonsten den leeren String.
func (q Question) QuestionText() string {
	if !q.IsValid() {
		return ""
	}
	return q.questionText
}

// Liefert die Anzahl der Antwortmöglichkeiten, wenn die Frage gültig ist.
// Liefert ansonsten -1.
func (q Question) ChoiceCount() int {
	if !q.IsValid() {
		return -1
	}
	return len(q.answerChoices)
}

// Liefert die korrekte Antwort, falls die Frage gültig ist.
// Liefert ansonsten den leeren String.
func (q Question) CorrectAnswer() string {
	if !q.IsValid() {
		return ""
	}
	return q.answerChoices[q.correctAnswer]
}

// Liefert einen String, der den Quiz-Teilnehmern vorgestellt werden kann, um eine Frage zu stellen.
// Der String enthält die Frage und die Antwortmöglichkeiten, wobei die korrekte Antwort markiert ist.
// Liefert den leeren String, falls die Frage ungültig ist.
func (q Question) ReadOut() string {
	if !q.IsValid() {
		return ""
	}

	firstLetter := int('A')
	result := []string{q.QuestionText()}
	for i, choice := range q.answerChoices {
		marker := func() string {
			if i == q.correctAnswer {
				return "->"
			} else {
				return "  "
			}
		}
		result = append(result, fmt.Sprintf(" %s %c: %s", marker(), firstLetter+i, choice))
	}
	result = append(result, "")

	return strings.Join(result, "\n")
}

// Erzeugt eine neue Frage.
func MakeQuestion(questionText string, correctAnswer int, answerChoices ...string) Question {
	return Question{questionText, answerChoices, correctAnswer}
}
