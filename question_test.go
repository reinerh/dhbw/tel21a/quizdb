package main

import "fmt"

func validQuestion1() Question {
	return MakeQuestion(
		"Was ist eine Katze?", 2,
		"Eine Maus", "Ein bösartiges Raubtier", "Ein knuddeliges Haustier", "Etwas anderes",
	)
}

func invalidQuestion1() Question {
	return MakeQuestion(
		"Was ist eine Katze?", 5,
		"Eine Maus", "Ein bösartiges Raubtier", "Ein knuddeliges Haustier", "Etwas anderes",
	)
}

func ExampleValidMCQuestion() {
	q1 := validQuestion1()

	fmt.Println(q1.IsValid())
	fmt.Println(q1.QuestionText())
	fmt.Println(q1.ChoiceCount())
	fmt.Println(q1.CorrectAnswer())

	// Output:
	// true
	// Was ist eine Katze?
	// 4
	// Ein knuddeliges Haustier
}

func ExampleInvalidMCQuestion() {
	q1 := invalidQuestion1()

	fmt.Println(q1.IsValid())
	fmt.Println(q1.QuestionText())
	fmt.Println(q1.ChoiceCount())
	fmt.Println(q1.CorrectAnswer())

	// Output:
	// false
	//
	// -1
	//
}

func ExampleReadOutValidMCQuestion() {
	q1 := validQuestion1()

	fmt.Println(q1.ReadOut())

	// Output:
	// Was ist eine Katze?
	//     A: Eine Maus
	//     B: Ein bösartiges Raubtier
	//  -> C: Ein knuddeliges Haustier
	//     D: Etwas anderes
}
